import React from 'react';
import $ from 'jquery';
import Header from './header.component.js'
import projectImg from '../images/project.png'
import issueImg from '../images/issue.png'
import commentImg from '../images/comment.png'

$(document).ready(function() {

	goToHomePage()
	smoothScroll()
	
	function goToHomePage() {
		$('.logo').click(function() {
				window.location.href = process.env.PUBLIC_URL
		})
	}
	
	function smoothScroll() {
		$(".home-arrow-1").on('click', function(event) {
		  if (this.hash !== "") {
		    event.preventDefault()
		    var hash = this.hash
		    $('html, body').animate({
		      scrollTop: $(hash).offset().top
		    }, 600, function() {
		      window.location.hash = hash
		    })
		  }
		})
	}
})

export default function Home() {
	return (
		<div>
			<div className='home-background' id='home-top'>
				<Header />
				<div className='home-section-1'>
					<h1 className='home-h1'>Get Things Done</h1>
					<hr />
					<p className='home-p-1'>With this simple yet productive issue tracker, you and your team can manage projects and tasks quickly and effectively!</p>
					<br />
					<a className='home-arrow-1' href='#home-middle'>
						Learn More <span className="chevron bottom"></span>
					</a>
				</div>
			</div>

			<div id='home-middle'>
				<h2>It's as easy as 1-2-3!</h2>
				<br />
				<div className='home-steps'>
					<div className='home-step-1'>
						<img src={projectImg} alt='project icon' />
						<br /><br />
						<p>Create a project</p>
						<hr />
						<p>Establish the purpose and objectives of a project in a central location.</p>
					</div>
					<span className="chevron right"></span>
					<div className='home-step-2'>
						<img src={issueImg} alt='issue icon' />
						<br /><br />
						<p>Create an issue ticket</p>
						<hr />
						<p>Document issues experienced and tasks to be completed throughout the duration of the project.</p>
					</div>
					<span className="chevron right"></span>
					<div className='home-step-3'>
						<img src={commentImg} alt='comment icon' />
						<br /><br />
						<p>Comment as you work</p>
						<hr />
						<p>Track the progress of issues and collaborate with team members more effectively.</p>
					</div>
				</div>
			</div>

			<div className='home-bottom'>
				<p>What are you waiting for? Let's get started.</p>
				<br /><br />
				<a className='home-arrow-1 gray-border' href='/signup'>
					Sign Up
				</a>
				<br /><br /><br />
				<hr />
				<footer>
					<p className='home-created-by'>Created by Peter Tran</p>
					<br />
					<a className='home-arrow-1 home-back-to-top' href='#home-top'>
						Back to Top <span className="chevron"></span>
					</a>
				</footer>
			</div>
		</div>
	)
}
