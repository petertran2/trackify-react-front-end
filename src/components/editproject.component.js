import React, { useState } from 'react'
import Header from './header.component.js'
import ProjectService from '../services/project.service.js'

export default function EditProject({ history, location }) {
	const [name, setName] = useState(location.state.name)
	const [status, setStatus] = useState(location.state.status)
	const [description, setDescription] = useState(location.state.description)
	const [errors, setErrors] = useState([])
	const [loading, setLoading] = useState(false)
	
	const onChangeName = (e) => {
		setName(e.target.value)
	}
	
	const onChangeStatus = (e) => {
		setStatus(e.target.value)
	}
	
	const onChangeDescription = (e) => {
		setDescription(e.target.value)
	}

	const vName = (name) => {
		if (!name) {
			setErrors(prevErrors => [...prevErrors, 'Project name is required'])
		}
	}
	
	const handleSubmit = (e) => {
		e.preventDefault()
		setErrors([])
		setLoading(true)
		vName(name)
		if (errors.length === 0) {
			ProjectService.editProject(location.state.id, name, status, description, location.state.creatorId).then(response => {
				setErrors([])
				setLoading(false)
				history.push(`/projects/${response.data.id}`)
			}, error => {
				setErrors(prevErrors => [...prevErrors, 'Unable to edit project. Please try again later.'])
				setLoading(false)
			})
		} else {
			setLoading(false)
		}
	}
	
	const handleCancel = (e) => {
		e.preventDefault()
		history.push(`/projects/${location.state.id}`)
	}

	return (
		<div>
			<Header />
			<div className='new-project-div'>
				<h1 className='h1'>Edit Project</h1>
				
				{errors && errors.map(error => (
					<div>
						<p className='auth-missing new-project-missing'>{ error }</p>
						<br />
					</div>
				))}
				
				{loading && (
					<div className='loading'>
						<div className="lds-dual-ring"></div>
					</div>
				)}
				
				<form id={`edit_project_${location.state.id}`} className='edit_project' onSubmit={handleSubmit}>
					<label htmlFor='project_name'>Name</label>
					<br />
					<input id='project_name' type='text' name='name' onChange={onChangeName} value={name} />
					<br />
					<label htmlFor='project_status'>Status</label>
					<br />
					<select value={status} name='status' onChange={onChangeStatus}>
						<option value='Development'>Development</option>
						<option value='Test'>Test</option>
						<option value='Production'>Production</option>
						<option value='Obsolete'>Obsolete</option>
					</select>
					<br />
					<label htmlFor='project_description'>Description</label>
					<br />
					<textarea id='project_description' name='description' onChange={onChangeDescription} value={ description }></textarea>
					<br />
					<input type='submit' value='Update Project' />
				</form>
				
				<form className='button_to' onSubmit={handleCancel}>
					<input type='submit' value='Cancel' />
				</form>
			</div>
		</div>
	)
}
