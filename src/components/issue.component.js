import React, { useState, useEffect } from 'react'
import Header from './header.component.js'
import IssueService from '../services/issue.service.js'
import UserService from '../services/user.service.js'
import ProjectService from '../services/project.service.js'
import CommentService from '../services/comment.service.js'
import AuthService from '../services/auth.service.js'
import moment from 'moment'
import axios from 'axios'

export default function Issue({ history, location }) {
	const issueid = Number(location.pathname.split('/')[2])
	const [issue, setIssue] = useState({})
	const [project, setProject] = useState({})
	const [users, setUsers] = useState([])
	const [comments, setComments] = useState([])
	const [content, setContent] = useState('')
	const [loading, setLoading] = useState(false)
	
	const issueEdit = (e) => {
		e.preventDefault()
		history.push(`/issues/${issueid}/edit`, {...issue})
	}
	
	const issueDelete = (e) => {
		e.preventDefault()
		IssueService.deleteIssue(issueid).then(() => {
			history.push('/dashboard')
		})
	}
	
	const onChangeContent = (e) => {
		setContent(e.target.value)
	}
	
	const handleComment = (e) => {
		e.preventDefault()
		setLoading(true)
		CommentService.postComment(content, AuthService.getCurrentUser().id, issueid)
			.then(response => {
				setComments(prevComments => [...prevComments, response.data])
				setContent('')
				setLoading(false)
			})
	}
	
	useEffect(() => {
		setLoading(true)
		axios.all([
			IssueService.getIssues(),
			UserService.getUsers(),
			ProjectService.getProjects(),
			CommentService.getComments()
		]).then(axios.spread((iResponse, uResponse, pResponse, cResponse) => {
			const issueresponse = iResponse.data.find(x => x.id === issueid)
			setIssue(issueresponse)
			setUsers(uResponse.data)
			setProject(pResponse.data.find(x => x.id === issueresponse.projectId))
			setComments(cResponse.data.filter(x => x.issueId === issueid))
			setLoading(false)
		}))
	}, [issueid])
	
	return (
		<div>
			<Header />
			
			{loading && (
				<div className='loading'>
					<div className="lds-dual-ring"></div>
				</div>
			)}
			
			<div className='a-project-div'>
				<h1 className='h1'>{ issue.subject }</h1>
				<div className='a-project-header'>
					<p>Project: { project.name }</p>
					<p>Created on { new Date(issue.createDate).toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' }) } / 
						Modified { moment(issue.updateDate).fromNow() }</p>
					<div className='a-project-buttons'>
						<form className='button_to' onSubmit={issueEdit}>
							<input type='submit' value='Edit Issue' />
						</form>
						<form className='button_to' onSubmit={issueDelete}>
							<input type='submit' value='Delete Issue'
								disabled={issue.reporterId !== AuthService.getCurrentUser().id} />
						</form>
					</div>
				</div>
				<br />
				<div className='a-project-header'>
					<p>Reporter: { users.find(user => user.id === issue.reporterId) && users.find(user => user.id === issue.reporterId).username }</p>
					<p>Assigned To: { issue.assigneeId && users.find(user => user.id === issue.assigneeId) ? users.find(user => user.id === issue.assigneeId).username : 'N/A' }</p>
					<p>Priority: { issue.priority }</p>
					<p>Status: { issue.status }</p>
					<p>Due Date: { issue.dueDate ? new Date(issue.dueDate).toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' }) : 'N/A' }</p>
				</div>
				<hr />
				<p className='a-project-description paragraph'>{ issue.description }</p>
			</div>
			<br />

			<div className='a-project-div'>
				<hr />
				<div className='dash-projects-header'>
					<div></div>
					<h2>Comments</h2>
					<div></div>
				</div>
				
				<div id='comments-section'>
					{comments.length !== 0 && comments.map(comment => (
						<div key={comment.id}>
							<div className='comment-header'>
								<p>{ users.find(user => user.id === comment.commenterId) && users.find(user => user.id === comment.commenterId).username } says,</p>
								<p>{ comment.createDate && new Date(comment.createDate).toLocaleString(undefined, { dateStyle: 'full' }) }</p>
							</div>
							<p className='comment-content paragraph'>{ comment.content }</p>
							<hr />
						</div>
					))}
					{comments.length === 0 && (
						<p className='no-comments'>No comments yet</p>
					)}
					<label className='new-comment-label'>New Comment</label>
					<br />
					<textarea onChange={onChangeContent} value={content}></textarea>
					<br />
					<button onClick={handleComment}>Add Comment</button>
				</div>
				
			</div>
		</div>
	)
}
