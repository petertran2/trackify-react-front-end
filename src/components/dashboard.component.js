import React, { useState, useEffect } from 'react'
import Header from './header.component.js'
import AuthService from '../services/auth.service.js'
import ProjectService from '../services/project.service.js'
import IssueService from '../services/issue.service.js'
import UserService from '../services/user.service.js'
import { Link } from 'react-router-dom'

export default function Dashboard({ history }) {
	const [projects, setProjects] = useState([])
	const [issues, setIssues] = useState([])
	const [users, setUsers] = useState([])
	const [error, setError] = useState(false)
	const [loading, setLoading] = useState(false)

	const newProject = (e) => {
		e.preventDefault()
		history.push('/projects/new')
	}
	
	const newIssue = (e) => {
		e.preventDefault()
		history.push('/issues/new')
	}
	
	useEffect(() => {
		setError(false)
		setLoading(true)
		ProjectService.getProjects().then(
			response => {
				setProjects(response.data)
				IssueService.getIssues().then(
					response => {
						setIssues(response.data)
						UserService.getUsers().then(
							response => {
								setUsers(response.data)
								setLoading(false)
							}, error => {
								setError(true)
								setLoading(false)
							}
						)
					}, error => {
						setError(true)
						setLoading(false)
					}
				)
			}, error => {
				setError(true)
				setLoading(false)
			}
		)
	}, [])

	return (
		<div>
			<Header />
			<div className='dash-header'>
				<h1 className='dash-h1'>Welcome, {AuthService.getCurrentUser().username}</h1>
				{error && (
					<div>
						<p className='auth-missing'>Unable to get projects and/or issues list. Please try again later.</p>
						<br />
					</div>
				)}
				{loading && (
					<div className='loading'>
						<div className="lds-dual-ring"></div>
					</div>
				)}
				<hr />
			</div>

			<div className='dash-container'>

				<div className='dash-projects'>
					<div className='dash-projects-header'>
						<div></div>
						<h2>Projects</h2>
						<form className='button_to' onSubmit={newProject}>
							<input type='submit' value='+' />
						</form>
					</div>
					<hr />
					<ul>
						{projects.map(project => (
							<li key={project.id}>
								<Link className='dash-project-link' to={`/projects/${project.id}`}>
									{ project.name }
								</Link>
								<p className='dash-project-description'>{ project.description }</p>
								<hr />
							</li>
						))}
					</ul>
					<Link className='dash-create-link dash-project-link' to='/projects/new'>
						Create New Project
					</Link>
				</div>
				
				<div className='dash-projects'>
					<div className='dash-projects-header'>
						<div></div>
						<h2>Issues</h2>
						<form className='button_to' onSubmit={newIssue}>
							<input type='submit' value='+' />
						</form>
					</div>
					<hr />
					<ul>
						{issues.map(issue => (
							<li key={issue.id}>
								<Link className='dash-project-link' to={`/issues/${issue.id}`}>
									{ issue.subject }
								</Link>
								<p className='dash-project-description'>
									Status: { issue.status } <span className='dash-separator'>| </span>
									Priority: { issue.priority } <span className='dash-separator'>| </span>
									Reported By: { users.find(user => user.id === issue.reporterId) && users.find(user => user.id === issue.reporterId).username }
								</p>
								<hr />
							</li>
						))}
					</ul>
					<Link className='dash-create-link dash-project-link' to='/issues/new'>
						Create New Issue
					</Link>
				</div>
				
			</div>
		</div>
	)
}
