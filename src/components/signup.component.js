import React, { useState } from 'react'
import Header from './header.component.js'
import AuthService from '../services/auth.service.js'
import { isEmail } from "validator";

export default function Signup({ history }) {
	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')
	const [email, setEmail] = useState('')
	const [confirmpassword, setConfirmpassword] = useState('')
	const [loading, setLoading] = useState(false)
	const [errors, setErrors] = useState([])
	
	const onChangeUsername = (e) => {
		setUsername(e.target.value)
	}
	
	const onChangePassword = (e) => {
		setPassword(e.target.value)
	}
	
	const onChangeEmail = (e) => {
		setEmail(e.target.value)
	}
	
	const onChangeConfirmpassword = (e) => {
		setConfirmpassword(e.target.value)
	}
	
	const vEmail = (email) => {
		if (!isEmail(email)) {
			setErrors(prevErrors => [...prevErrors, 'Invalid email'])
		}
	}
	
	const vUsername = (username) => {
		if (username.length < 3 || username.length > 20) {
			setErrors(prevErrors => [...prevErrors, 'Username must be between 3 and 20 characters'])
		}
	}
	
	const vPassword = (password) => {
		if (password.length < 6 || password.length > 40) {
			setErrors(prevErrors => [...prevErrors, 'Password must be between 6 and 40 characters'])
		}
	}
	
	const vConfirmpassword = (password, confirmpassword) => {
		if (password !== confirmpassword) {
			setErrors(prevErrors => [...prevErrors, 'Passwords do not match'])
		}
	}
	
	const handleSubmit = (e) => {
		e.preventDefault()
		setErrors([])
		setLoading(true)
		
		vEmail(email)
		vUsername(username)
		vPassword(password)
		vConfirmpassword(password, confirmpassword)
		
		if (errors.length === 0) {
			AuthService.register(username, email, password)
				.then(() => {
					AuthService.login(username, password).then(() => {
						setErrors([])
						setLoading(false)
						history.push('/dashboard')
					}, error => {
						setErrors(prevErrors => [...prevErrors, 'Unable to login. Please try again later.'])
						setLoading(false)
					})
				}, error => {
					setErrors(prevErrors => [...prevErrors, 'Unable to register. Please try again later.'])
					setLoading(false)
				})
		} else {
			setLoading(false)
		}
	}
	
	return (
		<div>
			<Header />
			<div className='auth-div'>
				<h1 className='auth-h1'>Sign Up</h1>
				
				{errors && errors.map(error => (
					<div>
						<p className='auth-missing'>{ error }</p>
						<br />
					</div>
				))}
				
				{loading && (
					<div className='loading'>
						<div className="lds-dual-ring"></div>
					</div>
				)}
				
				<form id='new_user' className='new_user' onSubmit={handleSubmit}>
					<input id='user_username' placeholder='Username' type='text' name='username' onChange={onChangeUsername} />
					<br />
					<input id='user_email' placeholder='Email Address' type='text' name='email' onChange={onChangeEmail} />
					<br />
					<input id='user_password' placeholder='Password' type='password' name='password' onChange={onChangePassword} />
					<br />
					<input id='user_password_confirmation' placeholder='Confirm Password' type='password' name='password_confirmation' onChange={onChangeConfirmpassword} />
					<br />
					<input type='submit' value='Create Account' />
				</form>
			</div>

			<footer className='home-created-by auth-footer'>Created by Peter Tran</footer>
		</div>
	)
}
