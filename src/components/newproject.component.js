import React, { useState } from 'react'
import Header from './header.component.js'
import AuthService from '../services/auth.service.js'
import ProjectService from '../services/project.service.js'

export default function NewProject({ history }) {
	const [name, setName] = useState('')
	const [status, setStatus] = useState('Development')
	const [description, setDescription] = useState('')
	const [errors, setErrors] = useState([])
	const [loading, setLoading] = useState(false)
	
	const onChangeName = (e) => {
		setName(e.target.value)
	}
	
	const onChangeStatus = (e) => {
		setStatus(e.target.value)
	}
	
	const onChangeDescription = (e) => {
		setDescription(e.target.value)
	}

	const vName = (name) => {
		if (!name) {
			setErrors(prevErrors => [...prevErrors, 'Project name is required'])
		}
	}
	
	const handleSubmit = (e) => {
		e.preventDefault()
		setErrors([])
		setLoading(true)
		vName(name)
		if (errors.length === 0) {
			ProjectService.postProject(name, status, description, AuthService.getCurrentUser().id).then(response => {
				setErrors([])
				setLoading(false)
				history.push(`/projects/${response.data.id}`)
			}, error => {
				setErrors(prevErrors => [...prevErrors, 'Unable to create project. Please try again later.'])
				setLoading(false)
			})
		} else {
			setLoading(false)
		}
	}
	
	const handleCancel = (e) => {
		e.preventDefault()
		history.push('/dashboard')
	}

	return (
		<div>
			<Header />
			<div className='new-project-div'>
				<h1 className='h1'>New Project</h1>
				
				{errors && errors.map(error => (
					<div>
						<p className='auth-missing new-project-missing'>{ error }</p>
						<br />
					</div>
				))}
				
				{loading && (
					<div className='loading'>
						<div className="lds-dual-ring"></div>
					</div>
				)}
				
				<form id='new_project' className='new_project' onSubmit={handleSubmit}>
					<label htmlFor='project_name'>Name</label>
					<br />
					<input id='project_name' type='text' name='name' onChange={onChangeName} />
					<br />
					<label htmlFor='project_status'>Status</label>
					<br />
					<select value={status} name='status' onChange={onChangeStatus}>
						<option value='Development'>Development</option>
						<option value='Test'>Test</option>
						<option value='Production'>Production</option>
						<option value='Obsolete'>Obsolete</option>
					</select>
					<br />
					<label htmlFor='project_description'>Description</label>
					<br />
					<textarea id='project_description' name='description' onChange={onChangeDescription}></textarea>
					<br />
					<input type='submit' value='Create Project' />
				</form>
				
				<form className='button_to' onSubmit={handleCancel}>
					<input type='submit' value='Cancel' />
				</form>
			</div>
		</div>
	)
}
