import React, { useState, useEffect } from 'react'
import Header from './header.component.js'
import ProjectService from '../services/project.service.js'
import AuthService from '../services/auth.service.js'
import IssueService from '../services/issue.service.js'
import UserService from '../services/user.service.js'
import { Link } from 'react-router-dom'
import moment from 'moment'

export default function Project({ history, location }) {
	const projectid = Number(location.pathname.split('/')[2])
	const [project, setProject] = useState({name: '', createDate: new Date()})
	const [issues, setIssues] = useState([])
	const [users, setUsers] = useState([])
	const [loading, setLoading] = useState(false)
	
	const projectEdit = (e) => {
		e.preventDefault()
		history.push(`/projects/${projectid}/edit`, {...project})
	}
	
	const projectDelete = (e) => {
		e.preventDefault()
		ProjectService.deleteProject(projectid).then(() => {
			history.push('/dashboard')
		})
	}
	
	useEffect(() => {
		setLoading(true)
		ProjectService.getProjects().then(response => {
			setProject(response.data.find(x => x.id === projectid))
			IssueService.getIssues().then(response => {
				setIssues(response.data.filter(x => x.projectId === projectid))
				UserService.getUsers().then(response => {
					setUsers(response.data)
					setLoading(false)
				})
			})
		})
	}, [projectid])
	
	return (
		<div>
			<Header />
			
			{loading && (
				<div className='loading'>
					<div className="lds-dual-ring"></div>
				</div>
			)}
			
			<div className='a-project-div'>
				<h1 className='h1'>{ project.name }</h1>
				<div className='a-project-header'>
					<p>Status: { project.status }</p>
					<p>Created on { new Date(project.createDate).toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' }) } / 
						Modified { moment(project.updateDate).fromNow() }</p>
					<div className='a-project-buttons'>
						<form className='button_to' onSubmit={projectEdit}>
							<input type='submit' value='Edit Project' />
						</form>
						<form className='button_to' onSubmit={projectDelete}>
							<input type='submit' value='Delete Project'
								disabled={project.creatorId !== AuthService.getCurrentUser().id || issues.length !== 0} />
						</form>
					</div>
				</div>
				<hr />
				<p className='a-project-description paragraph'>{ project.description }</p>
			</div>
			<br />
			<div className='dash-projects a-project-div'>
				<hr />
				<div className='dash-projects-header'>
					<div></div>
					<h2>Issues</h2>
					<div></div>
				</div>
				<ul>
					{issues.map(issue => (
						<div key={issue.id}>
							<Link className='dash-project-link' to={`/issues/${issue.id}`}>
								{ issue.subject }
							</Link>
							<p className='dash-project-description'>
								Status: { issue.status } <span className='dash-separator'>| </span>
								Priority: { issue.priority } <span className='dash-separator'>| </span>
								Reported By: { users.find(user => user.id === issue.reporterId) && users.find(user => user.id === issue.reporterId).username }
							</p>
							<hr />
						</div>
					))}
				</ul>
				<Link className='dash-create-link dash-project-link' to={{ pathname: '/issues/new', state: { projectid }}}>
					Create New Issue
				</Link>
			</div>
		</div>
	)
}
