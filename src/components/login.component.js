import React, { useState } from 'react'
import Header from './header.component.js'
import AuthService from '../services/auth.service.js'

export default function Login({ history }) {
	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')
	const [error, setError] = useState(false)
	const [loading, setLoading] = useState(false)
	
	const onChangeUsername = (e) => {
		setUsername(e.target.value)
	}
	
	const onChangePassword = (e) => {
		setPassword(e.target.value)
	}

	const handleSubmit = (e) => {
		e.preventDefault()
		setError(false)
		setLoading(true)
		AuthService.login(username, password)
			.then(() => {
				setError(false)
				setLoading(false)
				history.push('/dashboard')
			}, error => {
				setError(true)
				setLoading(false)
			})
	}

	return (
		<div>
			<Header />
			<div className='auth-div'>
				<h1 className='auth-h1'>Log In</h1>
				
				{error && (
					<div>
						<p className='auth-missing'>One or more fields is missing or incorrect.</p>
						<br />
					</div>
				)}
				
				{loading && (
					<div className='loading'>
						<div className="lds-dual-ring"></div>
					</div>
				)}
				
				<form onSubmit={handleSubmit}>
					<input id='username' type='text' name='username' placeholder='Username' onChange={onChangeUsername} />
					<br />
					<input id='password' type='password' name='password' placeholder='Password' onChange={onChangePassword} />
					<br />
					<input type='submit' value='Log In' />
				</form>
				
			</div>
			<footer className='home-created-by auth-footer'>Created by Peter Tran</footer>
		</div>
	)
}
