import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import AuthService from "../services/auth.service"

function Header({ history, location }) {
	
	const logOut = (e) => {
		e.preventDefault()
    AuthService.logout()
    history.push('/login')
  }
  
  const logIn = (e) => {
  	e.preventDefault()
  	history.push('/login')
  }
  
  const signUp = (e) => {
  	e.preventDefault()
  	history.push('/signup')
  }
  
	return (
		<header className={location.pathname === '/' ? '' : 'header-background'}>
			<div className="hover-div">
				{location.pathname === '/' ? (
					<a className='logo' href={process.env.PUBLIC_URL}>Trackify</a>
				) : ['/login', '/signup'].includes(location.pathname) ? (
					<Link className='logo' to='/'>Trackify</Link>
				) : (
					<Link className='logo' to='/dashboard'>Trackify</Link>
				)}
			</div>
			<div className='header-div'>
				{AuthService.getCurrentUser() ? (
					<form className='button_to' onSubmit={logOut}>
						<input type='submit' value='Log Out' />
					</form>
				) : (
					<div className='account-buttons'>
						<form className='button_to' onSubmit={logIn}>
							<input type='submit' value='Log In' />
						</form>
						<form className='button_to' onSubmit={signUp}>
							<input type='submit' value='Sign Up' />
						</form>
					</div>
				)}
			</div>
		</header>
	)
}

export default withRouter(Header)
