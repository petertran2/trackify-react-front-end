import React, { useState, useEffect } from 'react'
import Header from './header.component.js'
import AuthService from '../services/auth.service.js'
import UserService from '../services/user.service.js'
import ProjectService from '../services/project.service.js'
import IssueService from '../services/issue.service.js'

export default function NewIssue({ history, location }) {
	const [projects, setProjects] = useState([])
	const [users, setUsers] = useState([])
	const [projectid, setProjectid] = useState((location.state && location.state.projectid) || 1)
	const [assigneeid, setAssigneeid] = useState(null)
	const [priority, setPriority] = useState('Medium')
	const [status, setStatus] = useState('New')
	const [duedate, setDuedate] = useState(null)
	const [subject, setSubject] = useState('')
	const [description, setDescription] = useState('')
	const [errors, setErrors] = useState([])
	const [loading, setLoading] = useState(false)
	
	const onChangeProjectid = (e) => {
		setProjectid(e.target.value)
	}
	
	const onChangeAssigneeid = (e) => {
		setAssigneeid(e.target.value === '' ? null : e.target.value)
	}
	
	const onChangePriority = (e) => {
		setPriority(e.target.value)
	}
	
	const onChangeStatus = (e) => {
		setStatus(e.target.value)
	}
	
	const onChangeDueDate = (e) => {
		setDuedate(e.target.value)
	}
	
	const onChangeSubject = (e) => {
		setSubject(e.target.value)
	}
	
	const onChangeDescription = (e) => {
		setDescription(e.target.value)
	}
	
	const vSubject = (subject) => {
		if (subject.length === 0) {
			setErrors(prevErrors => [...prevErrors, 'Subject is required'])
		}
	}
	
	const handleSubmit = (e) => {
		e.preventDefault()
		setErrors([])
		setLoading(true)
		vSubject(subject)
		if (errors.length === 0) {
			IssueService.postIssue(priority, subject, status, description, duedate, projectid, AuthService.getCurrentUser().id, assigneeid).then(response => {
				setErrors([])
				setLoading(false)
				history.push(`/issues/${response.data.id}`)
			}, error => {
				setErrors(prevErrors => [...prevErrors, 'Unable to create issue. Please try again later.'])
				setLoading(false)
			})
		} else {
			setLoading(false)
		}
	}

	const handleCancel = (e) => {
		e.preventDefault()
		history.push('/dashboard')
	}
	
	useEffect(() => {
		setErrors([])
		setLoading(true)
		ProjectService.getProjects().then(response => {
			setProjects(response.data)
			UserService.getUsers().then(response => {
				setUsers(response.data)
				setLoading(false)
			}, error => {
				setErrors(prevErrors => [...prevErrors, 'Unable to get users list.'])
				setLoading(false)
			})
		}, error => {
			setErrors(prevErrors => [...prevErrors, 'Unable to get projects list.'])
			setLoading(false)
		})
	}, [])
	

	return (
		<div>
			<Header />
			<div className='new-project-div'>
				<h1 className='h1'>New Issue</h1>
				
				{errors && errors.map(error => (
					<div>
						<p className='auth-missing new-project-missing'>{ error }</p>
						<br />
					</div>
				))}
				
				{loading && (
					<div className='loading'>
						<div className="lds-dual-ring"></div>
					</div>
				)}
				
				<form id='new_issue' className='new_issue' onSubmit={handleSubmit}>
					<div className='issue-form-line'>
						<div>
							<label htmlFor='issue_project_id'>Project</label>
							<br />
							<select id='issue_project_id' name='project_id' value={projectid} onChange={onChangeProjectid}>
								{projects.map(project => (
									<option key={project.id} value={project.id}>{project.name}</option>
								))}
							</select>
						</div>
						<div>
							<label>Reporter</label>
							<br />
							{ AuthService.getCurrentUser().username }
						</div>
						<div>
							<label htmlFor='issue_assignee_id'>Assigned To</label>
							<br />
							<select id='issue_assignee_id' name='assignee_id' onChange={onChangeAssigneeid}>
								<option value=''></option>
								{users.map(user => (
									<option key={user.id} value={user.id}>{user.username}</option>
								))}
							</select>
						</div>
						<div>
							<label htmlFor='issue_priority'>Priority</label>
							<br />
							<select id='issue_priority' name='priority' value={priority} onChange={onChangePriority}>
								<option value='Low'>Low</option>
								<option value='Medium'>Medium</option>
								<option value='High'>High</option>
								<option value='Urgent'>Urgent</option>
							</select>
						</div>
						<div>
							<label htmlFor='issue_status'>Status</label>
							<br />
							<select id='issue_status' name='status' value={status} onChange={onChangeStatus}>
								<option value='New'>New</option>
								<option value='Assigned'>Assigned</option>
								<option value='Feedback'>Feedback</option>
								<option value='Acknowledged'>Acknowledged</option>
								<option value='Resolved'>Resolved</option>
								<option value='Closed'>Closed</option>
							</select>
						</div>
						<div>
							<label htmlFor='issue_due_date'>Due Date</label>
							<br />
							<input id='issue_due_date' type='date' name='due_date' onChange={onChangeDueDate} />
						</div>
					</div>
					<label htmlFor='issue_subject'>Subject</label>
					<br />
					<input id='issue_subject' type='text' name='subject' onChange={onChangeSubject} />
					<br />
					<label htmlFor='issue_description'>Description</label>
					<br />
					<textarea id='issue_description' name='description' onChange={onChangeDescription}></textarea>
					<br />
					<input type='submit' value='Create Issue' />
				</form>
				
				<form className='button_to' onSubmit={handleCancel}>
					<input type='submit' value='Cancel' />
				</form>
			</div>
		</div>
	)
}
