import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from './components/home.component.js'
import Signup from './components/signup.component.js'
import Login from './components/login.component.js'
import Dashboard from './components/dashboard.component.js'
import NewProject from './components/newproject.component.js'
import NewIssue from './components/newissue.component.js'
import Project from './components/project.component.js'
import EditProject from './components/editproject.component.js'
import Issue from './components/issue.component.js'
import EditIssue from './components/editissue.component.js'

function App() {
  return (
    <div className="App">
      <BrowserRouter basename={process.env.PUBLIC_URL}>
      	<Switch>
		    	<Route exact path='/' component={Home} />
		    	<Route path='/signup' component={Signup} />
		    	<Route path='/login' component={Login} />
		    	<Route path='/dashboard' component={Dashboard} />
		    	<Route path='/projects/new' component={NewProject} />
		    	<Route path='/issues/new' component={NewIssue} />
		    	<Route path='/projects/:id/edit' component={EditProject} />
		    	<Route path='/projects/:id' component={Project} />
		    	<Route path='/issues/:id/edit' component={EditIssue} />
		    	<Route path='/issues/:id' component={Issue} />
      	</Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
