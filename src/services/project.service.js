import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://trackify-spring-boot.herokuapp.com/api/v1/projects/';

class ProjectService {
  getProjects() {
  	return axios.get(API_URL, { headers: authHeader() });
  }
  
  getProject(id) {
  	return axios.get(API_URL + id, { headers: authHeader() });
  }
  
  postProject(name, status, description, creatorId) {
  	return axios.post(API_URL, { name, status, description, creatorId }, 
  		{ headers: authHeader() })
  }
  
  editProject(id, name, status, description, creatorId) {
  	return axios.put(API_URL + id, { name, status, description, creatorId },
  		{ headers: authHeader() })
  }
  
  deleteProject(id) {
  	return axios.delete(API_URL + id, { headers: authHeader() })
  }
}

export default new ProjectService();
