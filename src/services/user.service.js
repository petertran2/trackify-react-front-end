import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://trackify-spring-boot.herokuapp.com/api/v1/users/';

class UserService {
  getUsers() {
  	return axios.get(API_URL, { headers: authHeader() });
  }
  
  getUser(id) {
  	return axios.get(API_URL + id, { headers: authHeader() });
  }
}

export default new UserService();
