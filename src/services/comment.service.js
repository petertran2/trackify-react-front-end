import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://trackify-spring-boot.herokuapp.com/api/v1/comments/';

class CommentService {
  getComments() {
  	return axios.get(API_URL, { headers: authHeader() });
  }
  
  postComment(content, commenterId, issueId) {
  	return axios.post(API_URL, { content, commenterId, issueId }, { headers: authHeader() })
  }
}

export default new CommentService();
