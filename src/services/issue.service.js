import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://trackify-spring-boot.herokuapp.com/api/v1/issues/';

class IssueService {
  getIssues() {
  	return axios.get(API_URL, { headers: authHeader() });
  }
  
  getIssue(id) {
  	return axios.get(API_URL + id, { headers: authHeader() });
  }
  
  postIssue(priority, subject, status, description, dueDate, projectId, reporterId, assigneeId) {
  	return axios.post(API_URL, { priority, subject, status, description, dueDate, projectId, reporterId, assigneeId }, { headers: authHeader() })
  }
  
  editIssue(id, priority, subject, status, description, dueDate, projectId, reporterId, assigneeId) {
  	return axios.put(API_URL + id, { priority, subject, status, description, dueDate, projectId, reporterId, assigneeId }, { headers: authHeader() })
  }
  
  deleteIssue(id) {
  	return axios.delete(API_URL + id, { headers: authHeader() })
  }
}

export default new IssueService();
