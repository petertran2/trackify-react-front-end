# Trackify Issue Tracker

![Trackify Home Page](https://gitlab.com/petertran2/trackify-react-front-end/-/raw/master/README_assets/trackify_react_demo.gif)

Trackify Issue Tracker is a React single-page application (SPA) that documents issues and software bugs to help people manage projects more effectively. This app consumes an API written with Spring Boot and can list, show, create, update, and delete data from it.

Link: https://petertran2.gitlab.io/trackify-react-front-end

**Demo account** to try out the app:

    Username: Demo
    Password: demo123

For more information about the Spring Boot API for Trackify, please visit [its repository here](https://gitlab.com/petertran2/trackify-spring-boot-api).

## Front End Technologies

- React
- Javascript (ES6+)
- jQuery
- Axios
- HTML
- CSS
- React Router
- Validator
- Moment.js
- NPM
- GitLab Pages (deployed with GitLab CI/CD)

## Back End Technologies

- Spring Boot
- Spring Framework
- Java 8
- JPA (Spring Data)
- PostgreSQL
- Maven
- JSON Web Tokens (JWT)
- Postman
- Eclipse
- Heroku

### Author
Peter Tran
